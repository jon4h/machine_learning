import pickle
import numpy as np


pkl_file = "addition_model.pkl"

with open(pkl_file, 'rb') as file:
    pickle_model = pickle.load(file)


in1 = int(input("n1: "))
in2 = int(input("n2: "))

test = np.array([in1, in2])

result = pickle_model.predict([test])

for x in range(len(result)):
    print(result[x])