import pandas as pd
import numpy as np
import sklearn
from sklearn import linear_model
import pickle

data = pd.read_csv("./data.csv", sep=";")

data = data[["n1", "n2", "r"]]

predict = "r"

x = np.array(data.drop([predict], 1))
y = np.array(data[predict])

linear = linear_model.LinearRegression()

linear.fit(x, y)



in1 = int(input("n1: "))
in2 = int(input("n2: "))

test = np.array([in1, in2])

result = linear.predict([test])

for x in range(len(result)):
    print(result[x])

dump = input("Would you like to export the model?[y/n] ")

if dump.lower() == "y":
    pkl_file = input("Modelname: ")
    with open(pkl_file, 'wb') as file:
        pickle.dump(linear, file)