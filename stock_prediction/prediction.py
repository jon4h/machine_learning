import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
plt.style.use("bmh")

df = pd.read_csv("NFLX.csv")

"""
# visulaize close price data
plt.figure(figsize=(16,8))
plt.title("Netflix")
plt.xlabel("Days")
plt.ylabel("Close Price USD")
plt.plot(df['Close'])
#plt.show()
"""

# create variable to predict "x" days out into the future
future_days = 25

# create a new column (target) shifted "x" units/days up
df['Prediction'] = df[['Close']].shift(-future_days)

df = df[['Close','Prediction']]

# create the feature data set (x) and convert it to a numpy array and remove the last "x" rows/days
x = np.array(df.drop(['Prediction'], 1))[:-future_days]

# create target data set (y) and convert it to a numpy array and get all of the target except of the last "x" rows/days
y = np.array(df['Prediction'])[:-future_days]

# split data into 75% training and 25% testing
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2)


# create model
# create decision tree regressor model
tree = DecisionTreeRegressor().fit(x_train, y_train)

"""
predictions = tree.predict(x_test)

for x in range(len(predictions)):
    print(predictions[x], x_test[x], y_test[x])

plt.figure(figsize=(16,8))
plt.title("Model")
plt.xlabel("Days")
plt.ylabel("Close Price USD")   
plt.plot(predictions)
plt.plot(y_test)
plt.legend(['Validation', 'Prediction'])
plt.show()


"""

# get the last "x" rows of the feature dataset
x_future = df.drop(["Prediction"], 1)[:-future_days]
print(x_future)
x_future = x_future.tail(future_days)
print(x_future)
x_future = np.array(x_future)


# show the model tree prediction
print(x_future)
tree_prediction = tree.predict(x_future)


# visualite data tree
predictions = tree_prediction

valid = df[x.shape[0]:]
valid['Predictions'] = predictions
plt.figure(figsize=(16,8))
plt.title("Model")
plt.xlabel("Days")
plt.ylabel("Close Price USD")
plt.plot(df['Close'])
plt.plot(valid[['Close', 'Predictions']])
plt.legend(['Original Data', 'Validation', 'Prediction'])
plt.show()

print(valid)