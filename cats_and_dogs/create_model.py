import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.callbacks import TensorBoard
import numpy as np
import pickle
import time 

x = pickle.load(open("x.pickle", 'rb'))
y = pickle.load(open("y.pickle", 'rb'))

x = np.array(x/255.0)
y = np.array(y)

# Testing Runs
""" dense_layers = [0, 1, 2]
layer_sizes = [32, 64, 128]
conv_layers = [1, 2, 3] """

# best value accuracy
dense_layers = [0]
layer_sizes = [64]
conv_layers = [3]

for dense_layer in dense_layers:
    for layer_size in layer_sizes:
        for conv_layer in conv_layers:
            NAME = f"{conv_layer}-conv-{layer_size}-nodes-{dense_layer}-dense-{str(int(time.time()))}"

            tensorboard = TensorBoard(log_dir=f'logs\\{NAME}')

            model = Sequential()

            model.add(Conv2D(layer_size, (3,3), input_shape = x.shape[1:]))
            model.add(Activation("relu"))
            model.add(MaxPooling2D(pool_size=(2,2)))

            for l in range(conv_layer-1):
                model.add(Conv2D(layer_size, (3,3)))
                model.add(Activation("relu"))
                model.add(MaxPooling2D(pool_size=(2,2)))

            model.add(Flatten())

            for l in range(dense_layer):
                model.add(Dense(layer_size))
                model.add(Activation("relu"))


            model.add(Dense(1))
            model.add(Activation('sigmoid'))

            model.compile(loss="binary_crossentropy",
                optimizer="adam",
                metrics=['accuracy'])

            model.fit(x, y, batch_size=32, epochs=20, validation_split=0.3, callbacks=[tensorboard])

            # save model
            model.save(f"{NAME}.model")


