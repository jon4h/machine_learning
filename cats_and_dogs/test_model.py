import cv2
import tensorflow as tf
import numpy as np
import os

CATEGORIES = ["Dog", "Cat"]


def prepare(filepath):
    IMG_SIZE = 50
    img_array = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    return np.array(new_array).reshape(-1, IMG_SIZE, IMG_SIZE, 1)


model = tf.keras.models.load_model("3-conv-64-nodes-0-dense-1598837592.model")


for i in os.listdir("testing"):
    prediction = model.predict([prepare(f"testing\\{i}")])

    print(i, CATEGORIES[int(prediction[0][0])])
